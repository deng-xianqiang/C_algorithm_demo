# include <stdio.h>
# include <time.h>
# include <string.h>
# include "record.h"
# include <stdlib.h>
# include <io.h>
# include <windows.h>

void getNowTime(char* nowTime) {
    char acYear[50] = { 0 };
    char acMonth[50] = { 0 };
    char acDay[50] = { 0 };
    char acHour[50] = { 0 };
    char acMin[50] = { 0 };
    char acSec[50] = { 0 };

    time_t now = time(0);
    struct tm timenow;
    localtime_s(&timenow, &now);

    strftime(acYear, sizeof(acYear), "%Y", &timenow);
    strftime(acMonth, sizeof(acMonth), "%m", &timenow);
    strftime(acDay, sizeof(acDay), "%d", &timenow);
    strftime(acHour, sizeof(acHour), "%H", &timenow);
    strftime(acMin, sizeof(acMin), "%M", &timenow);
    strftime(acSec, sizeof(acSec), "%S", &timenow);

    strncat_s(nowTime, 50, acYear, 50);
    strncat_s(nowTime, 50, "-", 50);
    strncat_s(nowTime, 50, acMonth, 50);
    strncat_s(nowTime, 50, "-", 50);
    strncat_s(nowTime, 50, acDay, 50);
    strncat_s(nowTime, 50, "  ", 50);
    strncat_s(nowTime, 50, acHour, 50);
    strncat_s(nowTime, 50, ":", 50);
    strncat_s(nowTime, 50, acMin, 50);
    strncat_s(nowTime, 50, ":", 50);
    strncat_s(nowTime, 50, acSec, 50);
}

void record(char* new_money, char* old_money, int n) {

    char str1[50] = { 0 };  //存放日期字符串

    getNowTime(str1);
    char str2[20] = { 0 };  //存放账单文件名
    char str[200] = { 0 };

    FILE* fp = NULL;
    errno_t err = fopen_s(&fp, "1.txt", "r");

    if (fp == NULL) {
        fclose(fp);

        printf("打开文件失败！！！\n");
        exit(-1);
    }
    else {
        int i = 0;

        for (int j = 1; j <= 7 * n; j++) {
            fgets(str2, 20, fp);

            if (j == (7 * n) - 6) {
                break;
            }
        }

        fclose(fp);
        while (str2[i] != '\n') {
            str2[i] = str2[i];
            i++;
        }

        str2[i] = 0;
        strcat_s(str2, 20, ".txt");
    }

    int i = 0;
    while (old_money[i] != '\n') {
        old_money[i] = old_money[i];
        i++;
    }

    old_money[i] = 0;
    strcat_s(str, 200, str1);   //将时间字符串（str1）拷贝到
    strcat_s(str, 200, "  原始金额：");
    strcat_s(str, 200, old_money);
    strcat_s(str, 200, "  目前金额：");
    strcat_s(str, 200, new_money);
    strcat_s(str, 200, "\n");

    FILE* fpp = NULL;
    errno_t err1 = fopen_s(&fpp, str2, "a+");

    if (fpp == NULL) {
        fclose(fpp);

        printf("打开文件失败！！！\n");
        exit(-1);
    }

    fprintf(fpp, "%s", str);
    fclose(fpp);

    return;
}
