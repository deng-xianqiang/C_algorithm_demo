# include <stdio.h>
# include <stdlib.h>
# include "login_succeeded.h"
# include "deposit.h"			//存款 头文件
# include <windows.h>
# include "init_interface.h"	// 主页面
# include "withdraw_money.h"
# include "user_change_password.h"
# include "user_sign_in.h"
# include "lookup.h"

void login_succeeded(int n) {
	printf("*******************银行用户个人登录界面系统*******************\n");
	printf("\n");
	printf("\n                  1.存款\n");
	printf("\n                  2.取款\n");
	printf("\n                  3.余额查询\n");
	printf("\n                  4.账单查询\n");
	printf("\n                  5.修改密码\n");
	printf("\n                  6.返回上一级\n");
	printf("\n                  7.返回主系统界面\n");
	printf("\n");
	printf("\n*******************银行用户个人登录界面系统*******************\n");

	int a = 0;
	printf("\n请输入您要办理的业务：");
	scanf_s("%d", &a);

	system("cls");			//清屏

	switch (a) {
	case 1:
		deposit(n);
		break;
	case 2:
		withdraw_money(n);
		break;
	case 3:
		lookup_money(n);
		login_succeeded(n);
		break;
	case 4:
		lookup_bill(n);
		login_succeeded(n);
		break;
	case 5:
		user_change_password(n);
		break;
	case 6:
		user_sign_in();
		break;
	default:
		init_interface();	//误操作后返回主菜单栏
		break;
	}

	return;
}