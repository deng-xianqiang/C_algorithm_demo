# include <stdio.h>
# include <stdlib.h>
# include <windows.h>
# include "user_change_password.h"
# include "move.h"
# include "login_succeeded.h"
# include "user_sign_in.h"

void user_change_password(int n) {
	FILE* fp = NULL;
	FILE* fq = NULL;
	errno_t err1 = fopen_s(&fp, "1.txt", "r");
	errno_t err2 = fopen_s(&fq, "2.txt", "w+");

	if (fp == NULL || fq == NULL) {
		fclose(fp);
		fclose(fq);

		printf("打开文件失败！！！\n");
		exit(-1);
	}

	int flag = 0;
	int i = 0, j = 3;
	char password[288] = { 0 };
	char password_new[288] = { 0 };
	char str[288] = { 0 };

	printf("*******************银行个人账户信息修改系统*******************\n");
	printf("\n");
	printf("\n         请输入您原来的密码：");
	scanf_s("%s", password, 288);
	printf("\n");
	printf("\n");
	printf("\n");

	system("cls");

	strcat_s(password, 288, "\n");

	for (i = 1; i <= (n * 7) - 5; ++i) {
		++flag;
		fgets(str, 288, fp);

		if (i < (n * 7) - 5) {
			fprintf(fq, "%s", str);
		}

		if (i == (n * 7) - 5) {
			while (j > 0) {
				if (strcmp(password, str) == 0) {
					printf("*******************银行个人账户信息修改系统*******************\n");
					printf("\n");
					printf("\n         密码验证通过，正在跳转!!!\n");
					printf("\n");
					printf("\n");

					Sleep(500);
					system("cls");

					printf("*******************银行个人账户信息修改系统*******************\n");
					printf("\n");
					printf("\n         请输入您修改后的密码：");
					scanf_s("%s", password_new, 288);
					printf("\n");
					printf("\n");
					printf("\n");

					system("cls");

					strcat_s(password_new, 288, "\n");
					fprintf(fq, "%s", password_new);
					j = 3;

					printf("*******************银行个人账户信息修改系统*******************\n");
					printf("\n");
					printf("\n        密码修改成功，请重新登陆，即将跳转到登陆界面！！！\n");
					printf("\n");
					printf("\n*******************银行个人账户信息修改系统*******************");

					Sleep(650);
					system("cls");

					break;
				}
				else {
					printf("*******************银行个人账户信息修改系统*******************\n");
					printf("\n");
					printf("\n         密码验证失败，请确认后重新输入！！！\n");
					printf("\n");
					printf("\n*******************银行个人账户信息修改系统*******************");

					--j;

					Sleep(650);
					system("cls");

					if (j != 0) {
						printf("*******************银行个人账户信息修改系统*******************\n");
						printf("\n");
						printf("\n         请输入您原来的密码：");
						scanf_s("%s", password, 288);
						printf("\n");
						printf("\n");
						printf("\n");

						system("cls");

						strcat_s(password, 288, "\n");
					}
				}
			}
		}
	}

	if (j != 0) {
		while (!feof(fp)) {
			++flag;
			fgets(str, 288, fp);
			fprintf(fq, "%s", str);
		}

		fclose(fp);
		fclose(fq);

		move(flag);    //将2.txt文件中的数据移动到1.txt（覆盖操作）

		user_sign_in();
	}
	else {
		printf("*******************银行个人账户信息修改系统*******************\n");
		printf("\n");
		printf("\n      密码输入错误次数过多，今日已达上限，请明天再来！！！\n");
		printf("\n");
		printf("\n*******************银行个人账户信息修改系统*******************");

		Sleep(600);
		system("cls");

		fclose(fp);
		fclose(fq);

		login_succeeded(n);
	}

	return;
}