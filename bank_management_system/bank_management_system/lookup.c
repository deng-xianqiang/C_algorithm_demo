# include <stdio.h>
# include <string.h>
# include <windows.h>
# include "lookup.h"

//查找剩余金额
void lookup_money(int n) {
	FILE* fp = NULL;
	errno_t err = fopen_s(&fp, "1.txt", "r");

	char str1[200] = { 0 };
	char str[20] = { 0 };

	if (fp == NULL){
		fclose(fp);

		printf("打开文件失败！！！\n");
		exit(-1);
	}

	int i = 0;
	for (i = 1; i <= 7 * n; i++) {
		fgets(str1, 200, fp);
		if (i == 7 * n) {
			strcpy_s(str, 20, str1);
			print_money(str);
			break;
		}
	}
	fclose(fp);

	return;
}

//查找账单
void lookup_bill(int n) {
	FILE* fp = NULL;
	errno_t err = fopen_s(&fp, "1.txt", "r");

	char str1[200] = { 0 };
	char str[20] = { 0 };

	if (fp == NULL) {
		fclose(fp);

		printf("打开文件失败！！！\n");
		exit(-1);
	}

	for (int j = 1; j <= 7 * n; j++) {
		fgets(str, 20, fp);

		if (j == (7 * n) - 6) {
			break;
		}
	}

	int i = 0;
	while (str[i] != '\n') {
		str[i] = str[i];
		i++;
	}

	str[i] = '\0';
	i = 0;
	strcat_s(str, 20, ".txt");
	fclose(fp);

	FILE* fpp = NULL;
	errno_t err1 = fopen_s(&fpp, str, "r");

	printf("*******************银行个人开户系统*******************\n");
	printf("\n");
	printf("账单：\n");
	printf("\n");
	int flag = 0;

	while (!feof(fpp)) {
		fgets(str1, 200, fpp);
		flag++;
	}

	fclose(fpp);

	FILE* fppp = NULL;
	errno_t err2 = fopen_s(&fppp, str, "r");

	flag--;

	while (!feof(fppp)) {
		flag--;
		++i;
		fgets(str1, 200, fppp);
		printf(" %d: %s\n", i, str1);

		if (flag == 0) {
			break;
		}
	}

	fclose(fppp);

	printf("\n");
	printf("\n");
	printf("*******************银行个人开户系统*******************\n");

	Sleep(4000);
	system("cls");

	return;
}

void print_money(char* str)
{
	printf("*******************银行个人开户系统*******************\n");
	printf("\n");
	printf("\n");
	printf("\n");
	printf("          当前存款：%s\n", str);
	printf("\n");
	printf("\n");
	printf("\n");
	printf("*******************银行个人开户系统*******************\n");
	Sleep(1000);
	system("cls");

	return;
}