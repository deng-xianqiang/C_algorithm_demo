# include <stdio.h>
# include <windows.h>
# include "init_interface.h"
# include "user_open_the_interface.h"
# include "user_sign_in.h"
# include "user_register.h"
# include "user_exit_interface.h"
# include "information_input.h"

void init_interface(void) {
	user_open_the_interface();//用户开始使用界面
	printf("\n----->请输入您要办理的业务：");

	int a = 0;
	scanf_s("%d", &a);

	system("cls");	   //清屏

	switch (a) {
	case 1:
		user_sign_in();//用户登录
		break;
	case 2:
		user_register();//用户注册
		break;
	default:
		user_exit_interface();//用户退出系统界面
		break;
	}

	return;
}