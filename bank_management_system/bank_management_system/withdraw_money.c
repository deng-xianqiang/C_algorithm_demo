# include <stdio.h>
# include <stdlib.h>
# include <windows.h>
# include "withdraw_money.h"
# include "login_succeeded.h"
# include "move.h"
# include "record.h"

void withdraw_money(int n) {
	FILE* fp = NULL;
	FILE* fq = NULL;
	errno_t err1 = fopen_s(&fp, "1.txt", "r");
	errno_t err2 = fopen_s(&fq, "2.txt", "w+");

	if (fp == NULL || fq == NULL) {
		fclose(fp);
		fclose(fq);

		printf("打开文件失败！！！\n");
		exit(-1);
	}

	int deposit = 0, flag = 0, i = 0, money = 0;
	char str[200] = { 0 };
	char string[10] = { 0 };

	printf("*******************银行用户个人取款系统*******************\n");
	printf("\n");
	printf("\n         请输入您要取款的金额：");
	scanf_s("%d", &deposit);
	printf("\n");
	printf("\n");
	printf("\n");

	system("cls");

	char new_money[10] = { 0 };
	char old_money[10] = { 0 };

	for (i = 1; i <= 7 * n; i++) {
		++flag;
		fgets(str, 200, fp);

		if (i < 7 * n) {
			fprintf(fq, "%s", str);
		}

		if (i == 7 * n) {
			money = atoi(str);	  //将字符串数字转化为整型数据

			if (money < deposit) {
				printf("*******************银行用户个人取款系统*******************\n");
				printf("\n");
				printf("\n                   金额不足！！！ \n");
				printf("\n");
				printf("\n*******************银行用户个人取款系统*******************");

				Sleep(650);
				system("cls");

				fprintf(fq, "%s", str);
			}
			else {
				printf("*******************银行用户个人取款系统*******************\n");
				printf("\n");
				printf("\n                   取款成功！！！ \n");
				printf("\n");
				printf("\n*******************银行用户个人取款系统*******************");

				Sleep(650);
				system("cls");

				strcpy_s(old_money, 10, str);

				money = money - deposit;
				_itoa_s(money, string, 10, 10);

				strcpy_s(new_money, 10, string);

				fprintf(fq, "%s", string);
				fputc('\n', fq);
			}
		}
	}

	while (!feof(fp)) {
		++flag;
		fgets(str, 200, fp);
		fprintf(fq, "%s", str);
	}

	fclose(fp);
	fclose(fq);

	if (new_money != old_money) {
		record(new_money, old_money, n);//将新旧金额存入文件中
	}

	move(flag);    //将2.txt文件中的数据移动到1.txt（覆盖操作）

	login_succeeded(n);//返回用户个人登录界面

	return;
}