# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <windows.h>
# include "deposit.h"
# include "init_interface.h"
# include "move.h"
# include "login_succeeded.h"
# include "record.h"

void deposit(int n) {
	FILE* fp = NULL;
	FILE* fq = NULL;
	errno_t err1 = fopen_s(&fp, "1.txt", "r");
	errno_t err2 = fopen_s(&fq, "2.txt", "w+");

	if (fp == NULL || fq == NULL) {
		printf("打开文件失败！！！\n");
		exit(-1);
	}

	int flag = 0;
	int i;
	char str[200] = { 0 };
	int deposit = 0, money = 0;	//monery将字符串数字转化为整型数据
	char new_money[10] = { 0 }; //记录存款后金额
	char old_money[10] = { 0 };	//记录存款前金额

	printf("*******************银行用户个人存款系统*******************\n");
	printf("\n");
	printf("\n       请输入您要存储的金额数：");
	scanf_s("%d", &deposit);
	printf("\n");
	printf("\n");
	printf("\n");

	system("cls");

	printf("*******************银行用户个人存款系统*******************\n");
	printf("\n");
	printf("\n               请将现金整齐放入指定区域内！！！\n");
	printf("\n");
	printf("\n*******************银行用户个人存款系统*******************");

	Sleep(1200);
	system("cls");

	printf("*******************银行用户个人存款系统*******************\n");
	printf("\n");
	printf("\n                  机器正在验钞，请稍后！！！\n");
	printf("\n");
	printf("\n*******************银行用户个人存款系统*******************");

	Sleep(1200);
	system("cls");

	char string[10] = { 0 };

	printf("*******************银行用户个人存款系统*******************\n");
	printf("\n");
	printf("\n	              已完成存款！！！\n");
	printf("\n");
	printf("\n*******************银行用户个人存款系统*******************");

	Sleep(650);
	system("cls");

	for (i = 1; i <= 7 * n; i++) {
		++flag;
		fgets(str, 200, fp);

		if (i < 7 * n) {
			fprintf(fq, "%s", str);
		}

		if (i == 7 * n) {
			strcpy_s(old_money, 10, str);   //存款之前的金额

			money = atoi(str);	  //将字符串数字转化为整型数据
			money = money + deposit;

			_itoa_s(money, string, 10, 10);
			strcpy_s(new_money, 10, string);   //存款之前的金额

			fprintf(fq, "%s", string);
			fputc('\n', fq);
		}
	}

	while (!feof(fp)) {
		++flag;
		fgets(str, 200, fp);
		fprintf(fq, "%s", str);
	}

	fclose(fp);
	fclose(fq);

	if (new_money != old_money) {
		record(new_money, old_money, n);//将新旧金额存入文件中
	}

	move(flag);    //将2.txt文件中的数据移动到1.txt（覆盖操作）

	login_succeeded(n);//返回到用户个人登录界面

	return;
}