/*
任意给定一个数学表达式如 {5*(9-2)-[15-(8-3)/2]}+3*(6-4)，试设计一个算法判断表达式的括号是否匹配
用链表写一个栈，用来解决字符串中括号匹配的问题，并同时实现边入栈边出栈的操作
*/
# include <stdio.h>
# include <stdlib.h>
# include <string.h>

typedef char data_c;

typedef struct node {
	data_c data;
	struct node* pnext;
}NODE, * PNODE;
typedef struct stack {
	PNODE top;
	PNODE bottom;
}STACK, * PSTACK;

void stack_list_demo_1(void);//链式栈的演示函数
void init_stack(PSTACK);//初始化栈
void jugde_arr_en(PSTACK);//判断字符串中的括号，并对其进行符合预期的入栈和出栈相关操作
void en_stack(PSTACK, data_c);//入栈操作
void traversal_stack(PSTACK);//对栈进行遍历
void de_stack(PSTACK);//出栈操作
void jugde_match(PSTACK);//判断字符串内的所有括号时候真的匹配成功

int main(void)
{
	stack_list_demo_1();
	
	return 0;
}

void stack_list_demo_1(void) {
	STACK s;

	init_stack(&s);
	jugde_arr_en(&s);
	jugde_match(&s);
	traversal_stack(&s);
	
	return;
}

void init_stack(PSTACK p) {
	p->bottom = NULL;
	p->top = NULL;

	traversal_stack(p);

	return;
}

void jugde_arr_en(PSTACK p) {
	data_c arr[] = "{5*(9-2)-[15-(8-3)/2]}+3*(6-4)";
	int i = 0;
	int len = strlen(arr);

	for (i; i < len; ++i) {
		if (arr[i] == '(' || arr[i] == ')' || arr[i] == '[' ||
			arr[i] == ']' || arr[i] == '{' || arr[i] == '}') {
			if (p->top == NULL) {
				en_stack(p, arr[i]);
			}
			else{
				if (p->top->data + 2 == arr[i] + 1 ||
					p->top->data + 3 == arr[i] + 1 ||
					p->top->data + 2 == arr[i]) {
					de_stack(p);
				}
				else {
					en_stack(p, arr[i]);
				}
			}
		}
	}
}

void en_stack(PSTACK p, data_c arr) {
	PNODE pnow = NULL;
	pnow = (PNODE)malloc(sizeof(NODE));

	if (pnow == NULL) {
		printf("\n内存分配失败，程序终止！！！a\n");
		exit(-1);
	}
	pnow->data = arr;
	pnow->pnext = NULL;

	if (p->top == NULL) {
		p->top = pnow;
		p->bottom = pnow;
	}
	else {
		pnow->pnext = p->top;
		p->top = pnow;
	}

	printf("\n此时链式栈入栈成功，入栈元素为：%c\n", pnow->data);

	return;
}

void traversal_stack(PSTACK p) {
	if (p->top == NULL) {
		printf("\n此时链式栈的内容为空！！！\n");
	}
	else {
		PNODE q = p->top;

		printf("\n此时链式栈的内容为：");

		while (q != NULL) {
			printf("%c", q->data);
			q = q->pnext;
		}

		printf("\n");
	}

	return;
}

void de_stack(PSTACK p) {
	if (p->top == NULL) {
		printf("\n此时链式栈元素为空，不用出栈！！！\n");
	}
	else {
		PNODE q = p->top;

		p->top = p->top->pnext;

		printf("\n此时链式栈出栈成功，出栈元素为：%c\n", q->data);

		free(q);

		q = p->top;
	}

	return;
}

void jugde_match(PSTACK p) {
	if (p->top == NULL) {
		printf("\n此时字符串内的括号匹配成功！！！\n");
	}
	else {
		printf("\n此时字符串内的括号匹配不成功！！！\n");
	}
}

/*
运行结果：

此时链式栈的内容为空！！！

此时链式栈入栈成功，入栈元素为：{

此时链式栈入栈成功，入栈元素为：(

此时链式栈出栈成功，出栈元素为：(

此时链式栈入栈成功，入栈元素为：[

此时链式栈入栈成功，入栈元素为：(

此时链式栈出栈成功，出栈元素为：(

此时链式栈出栈成功，出栈元素为：[

此时链式栈出栈成功，出栈元素为：{

此时链式栈入栈成功，入栈元素为：(

此时链式栈出栈成功，出栈元素为：(

此时字符串内的括号匹配成功！！！

此时链式栈的内容为空！！！
*/