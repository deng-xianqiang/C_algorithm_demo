# include <stdio.h>
# include <stdlib.h>
# include <stdbool.h>

typedef char data_c;

typedef struct treenode {
	data_c data;//节点的数据
	struct treenode* pleftchild;//左孩子
	struct treenode* prightchild;//右孩子
	struct treenode* pnext;//递归层次遍历时使用
}TREENODE, * PTREENODE;

typedef struct queue {
	PTREENODE pfront;
	PTREENODE prear;
}QUEUE, * PQUEUE;

void tree_list_demo_1(void);//链式二叉树演示函数
bool judge_empty_treenode(PTREENODE);//判断二叉树的节点是否为空
int first_show_tree(PTREENODE);//先序遍历树
int middle_show_tree(PTREENODE);//中序遍历树
int after_show_tree(PTREENODE);//后序遍历树
int hierarchy_show_tree(PTREENODE, PQUEUE, int*, int*);//递归层次遍历树
PTREENODE creat_tree_list(void);//创建一个链式二叉树
void en_queuetree(PTREENODE ptree, PQUEUE p);//入队
void de_queue(PQUEUE);//出队
void init_queue(PQUEUE);//初始化队列
void traversal_queue(PQUEUE);//遍历队列元素

int main(void)
{
	tree_list_demo_1();

	return 0;
}

void tree_list_demo_1(void) {
	PTREENODE ptree = NULL;
	ptree = creat_tree_list();
	judge_empty_treenode(ptree);

	//先序遍历树
	printf("先序遍历树：\n");
	first_show_tree(ptree);

	//中序遍历树
	printf("\n中序遍历树：\n");
	middle_show_tree(ptree);

	//后序遍历树
	printf("\n后序遍历树：\n");
	after_show_tree(ptree);

	//递归层次遍历树
	QUEUE queuetree_list;
	PQUEUE p = &queuetree_list;
	init_queue(p);
	int i = 0, j = 0;
	printf("\n递归层次遍历树：\n");
	hierarchy_show_tree(ptree, p, &i, &j);

	return;
}

PTREENODE creat_tree_list(void) {
	PTREENODE ptreeA = (PTREENODE)malloc(sizeof(TREENODE));
	PTREENODE ptreeB = (PTREENODE)malloc(sizeof(TREENODE));
	PTREENODE ptreeC = (PTREENODE)malloc(sizeof(TREENODE));
	PTREENODE ptreeD = (PTREENODE)malloc(sizeof(TREENODE));
	PTREENODE ptreeE = (PTREENODE)malloc(sizeof(TREENODE));
	PTREENODE ptreeF = (PTREENODE)malloc(sizeof(TREENODE));
	PTREENODE ptreeG = (PTREENODE)malloc(sizeof(TREENODE));
	PTREENODE ptreeH = (PTREENODE)malloc(sizeof(TREENODE));
	PTREENODE ptreeI = (PTREENODE)malloc(sizeof(TREENODE));

	ptreeA->data = 'A';
	ptreeA->pleftchild = ptreeB;
	ptreeA->prightchild = ptreeC;

	ptreeB->data = 'B';
	ptreeB->pleftchild = ptreeD;
	ptreeB->prightchild = ptreeE;

	ptreeC->data = 'C';
	ptreeC->pleftchild = ptreeF;
	ptreeC->prightchild = ptreeG;

	ptreeD->data = 'D';
	ptreeD->pleftchild = NULL;
	ptreeD->prightchild = NULL;

	ptreeE->data = 'E';
	ptreeE->pleftchild = ptreeH;
	ptreeE->prightchild = NULL;

	ptreeF->data = 'F';
	ptreeF->pleftchild = NULL;
	ptreeF->prightchild = ptreeI;

	ptreeG->data = 'G';
	ptreeG->pleftchild = NULL;
	ptreeG->prightchild = NULL;

	ptreeH->data = 'H';
	ptreeH->pleftchild = NULL;
	ptreeH->prightchild = NULL;

	ptreeI->data = 'I';
	ptreeI->pleftchild = NULL;
	ptreeI->prightchild = NULL;

	return ptreeA;
}

bool judge_empty_treenode(PTREENODE ptree) {
	if (ptree == NULL) {
		printf("此时链式二叉树为空，无需遍历！！！\n");
		return false;
	}
	else {
		return true;
	}
}

int first_show_tree(PTREENODE ptree) {
	printf("%c ", ptree->data);

	if (ptree->pleftchild != NULL) {
		first_show_tree(ptree->pleftchild);
	}

	if (ptree->prightchild != NULL) {
		first_show_tree(ptree->prightchild);
	}

	return 0;
}

int middle_show_tree(PTREENODE ptree) {
	if (ptree->pleftchild != NULL) {
		first_show_tree(ptree->pleftchild);
	}

	printf("%c ", ptree->data);

	if (ptree->prightchild != NULL) {
		first_show_tree(ptree->prightchild);
	}

	return true;

	return 0;
}

int after_show_tree(PTREENODE ptree) {
	if (ptree->pleftchild != NULL) {
		first_show_tree(ptree->pleftchild);
	}

	if (ptree->prightchild != NULL) {
		first_show_tree(ptree->prightchild);
	}
	printf("%c ", ptree->data);

	return true;

	return 0;
}

int hierarchy_show_tree(PTREENODE ptree, PQUEUE p, int* i, int* j) {
	if (*j == 0) {
		en_queuetree(ptree, p);
	}

	if (ptree->pleftchild != NULL) {
		en_queuetree(ptree->pleftchild, p);
	}
	if (ptree->prightchild != NULL) {
		en_queuetree(ptree->prightchild, p);
	}
	de_queue(p);

	if (*i != 0) {
		return;
	}
	if (p->pfront == NULL) {
		return;
	}
	*j++;
	hierarchy_show_tree(p->pfront, p, i, j);
	*i++;

	//traversal_queue(p);

	if (p != NULL) {
		return;
	}

	return 0;
}

void init_queue(PQUEUE p) {
	p->pfront = NULL;
	p->prear = NULL;

	return;
}

void en_queuetree(PTREENODE ptree, PQUEUE p) {
	PTREENODE pnow = (PTREENODE)malloc(sizeof(TREENODE));
	pnow->data = ptree->data;
	pnow->pleftchild = ptree->pleftchild;
	pnow->prightchild = ptree->prightchild;
	pnow->pnext = NULL;

	if (p->pfront == NULL) {
		p->pfront = pnow;
		p->prear = p->pfront;
	}
	else {
		p->prear->pnext = pnow;
		p->prear = pnow;
	}

	return;
}

void de_queue(PQUEUE p) {
	if (p->pfront != NULL) {
		PTREENODE q = p->pfront;
		printf("%c ", p->pfront->data);
		p->pfront = p->pfront->pnext;
		free(q);
	}

	return;
}

void traversal_queue(PQUEUE p) {
	if (p->pfront == NULL) {
		printf("\n此时链式队列为空，无需遍历！！！\n");
	}
	else {
		PTREENODE q = p->pfront;
		printf("\n此时链式队列中元素为: ");

		while (q != NULL) {
			printf("%c ", q->data);
			q = q->pnext;
		}
	}

	printf("\n");

	return;
}
/*
运行结果：
先序遍历树：
A B D E H C F I G
中序遍历树：
B D E H A C F I G
后序遍历树：
B D E H C F I G A
递归层次遍历树：
A B C D E F G H I
*/