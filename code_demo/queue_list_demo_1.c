# include <stdio.h>
# include <stdlib.h>

typedef int data_t;
typedef struct node {
	data_t data;
	struct node* pnext;
}NODE, * PNODE;
typedef struct queue {
	PNODE pfront;
	PNODE prear;
}QUEUE, * PQUEUE;

void queue_demo_1(void);//队列演示函数
void init_queue(PQUEUE);//队列初始化
void en_queue(PQUEUE, data_t);//入队
void de_queue(PQUEUE, data_t*);//出队
void traversal_queue(PQUEUE);//对队列进行遍历
void queue_length(PQUEUE, int*);//计算队列长度
void destroy_queue(PQUEUE);//销毁队列

int main(void)
{
	queue_demo_1();
	
	return 0;
}

void queue_demo_1(void) {
	int length = 0;
	data_t n = 0;
	QUEUE queue_list;

	init_queue(&queue_list);
	traversal_queue(&queue_list);
	en_queue(&queue_list, 8);
	en_queue(&queue_list, 2);
	en_queue(&queue_list, 3);
	traversal_queue(&queue_list);
	de_queue(&queue_list, &n);
	en_queue(&queue_list, 5);
	traversal_queue(&queue_list);
	queue_length(&queue_list, &length);
	destroy_queue(&queue_list);
	queue_length(&queue_list, &length);

	return;
}

void init_queue(PQUEUE p) {
	p->pfront = NULL;
	p->prear = NULL;

	return;
}

void en_queue(PQUEUE p, data_t n) {
	PNODE pnow = (PNODE)malloc(sizeof(NODE));
	pnow->data = n;
	pnow->pnext = NULL;

	if (p->pfront == NULL) {
		p->pfront = pnow;
		p->prear = p->pfront;
	}
	else {
		p->prear->pnext = pnow;
		p->prear = pnow;
	}

	printf("\n此时链式队列入队成功，入队的元素为 %d \n", pnow->data);

	return;
}

void de_queue(PQUEUE p, data_t* n) {
	if (p->pfront == NULL) {
		printf("\n此时链式队列为空，无需出栈！！！\n");
	}
	else {
		PNODE q = p->pfront;
		*n = p->pfront->data;
		printf("\n此时链式队列出队成功，出队的元素为 %d \n", p->pfront->data);
		p->pfront = p->pfront->pnext;
		free(q);
	}

	return;
}

void traversal_queue(PQUEUE p) {
	if (p->pfront == NULL) {
		printf("\n此时链式队列为空，无需遍历！！！\n");
	}
	else {
		PNODE q = p->pfront;
		printf("\n此时链式队列中元素为: ");

		while (q != NULL) {
			printf("%d ", q->data);
			q = q->pnext;
		}
	}

	printf("\n");

	return;
}

void queue_length(PQUEUE p, int* len) {
	PNODE q = p->pfront;
	*len = 0;

	while (q != NULL) {
		(*len)++;
		q = q->pnext;
	}

	printf("\n此时的链式队列的长度为: %d\n", *len);

	return;
}

void destroy_queue(PQUEUE p) {
	PNODE q = p->pfront;
	while (p->pfront != NULL) {
		p->pfront = p->pfront->pnext;
		free(q);
		q = p->pfront;
	}

	return;
}
/*
运行结果：
此时链式队列为空，无需遍历！！！


此时链式队列入队成功，入队的元素为 8

此时链式队列入队成功，入队的元素为 2

此时链式队列入队成功，入队的元素为 3

此时链式队列中元素为: 8 2 3

此时链式队列出队成功，出队的元素为 8

此时链式队列入队成功，入队的元素为 5

此时链式队列中元素为: 2 3 5

此时的链式队列的长度为: 3

此时的链式队列的长度为: 0
*/