# include <stdio.h>
# include <stdlib.h>
# include <stdbool.h>

typedef int int_t;

typedef struct node {
	int_t data;
	struct node* pnext;
}NODE, * PNODE;

void list_demo_1(void);//链表演示函数 list_demo_1

void printf_char(char*);//打印字符串
PNODE creat_list(PNODE);//创建链表，并用户输入达到初始化
void stop_program(void);//停止程序
void judge_creat_space(PNODE);//输出对应返回判断malloc创建的空间的结果
bool creat_space(PNODE);//判断malloc创建的空间是否成功
int list_length(PNODE);//检测链表的长度
void output_list_length(PNODE);//打印链表的长度
void traverse_list(PNODE);//对链表进行遍历
void judge_is_empty_list(PNODE);//输出对应返回判断链表是否为空的结果
bool is_empty_list(PNODE);//判断链表是否为空
void user_insert_parameter(PNODE);//用户插入节点的各项参数
void judge_insert_list(PNODE, int, int_t);//输出对应返回判断链表插入节点是否成功的结果
bool insert_list(PNODE, int, int_t);//判断链表插入节点是否成功
void user_delete_parameter(PNODE);//用户删除节点的各项参数
void judge_delete_list(PNODE, int, int_t*);//输出对应返回判断链表删除节点是否成功的结果
bool delete_list(PNODE, int, int_t*);//判断链表删除节点是否成功
void sort_list(PNODE);//对链表的内容进行排序
int judge_destroy_list(PNODE);//输出对应返回判断链表格式化是否成功的结果
bool destroy_list(PNODE);//判断链表格式化是否成功

int main(void)
{
	list_demo_1();

	return 0;
}

void list_demo_1(void) {
	PNODE phead = NULL;

	traverse_list(phead);
	phead = creat_list(phead);
	output_list_length(phead);
	traverse_list(phead);
	judge_is_empty_list(phead);
	user_insert_parameter(phead);
	traverse_list(phead);
	user_delete_parameter(phead);
	traverse_list(phead);
	sort_list(phead);
	traverse_list(phead);
	if (judge_destroy_list(phead)) {
		phead = NULL;
	}
	traverse_list(phead);

	return;
}

void printf_char(char* p) {
	printf(p);

	return;
}

PNODE creat_list(PNODE phead) {
	phead = (PNODE)malloc(sizeof(NODE));

	judge_creat_space(phead);

	PNODE ptail = phead;
	ptail->pnext = NULL;

	int length = 0, i = 0;
	int_t val = 0;

	printf_char("\n请用户输入需要生成的链表节点的个数：");
	scanf_s("%d", &length);
	printf_char("\n");

	if (length <= 0) {
		printf_char("用户输入生成的节点个数不合法，程序终止！！！\n");

		free(phead);
		phead = NULL;

		stop_program();
	}
	else {
		for (i = 1; i <= length; ++i) {
			PNODE pnew = NULL;
			pnew = (PNODE)malloc(sizeof(NODE));

			judge_creat_space(pnew);

			printf("请输入链表第%d个节点的参数：", i);
			scanf_s("%d", &val);

			pnew->data = val;
			ptail->pnext = pnew;
			pnew->pnext = NULL;
			ptail = pnew;
		}
	}

	return phead;
}

void stop_program(void) {
	exit(-1);
}

void judge_creat_space(PNODE p) {
	if (creat_space(p)) {
		return;
	}
	else {
		printf_char("内存分配失败，程序终止！！！\n");

		stop_program();
	}
}

bool creat_space(PNODE p) {
	if (p != NULL) {
		return true;
	}
	else {
		return false;
	}
}

void output_list_length(PNODE p) {
	int length = list_length(p);

	printf("\n此时链表中的节点个数为：%d\n", length);

	return;
}

int list_length(PNODE p) {
	int length = 0;
	PNODE q = p;
	if (p == NULL) {
		length = 0;
	}
	else {
		while (q->pnext != NULL) {
			length++;
			q = q->pnext;
		}
	}

	return length;
}

void traverse_list(PNODE p) {
	if (p == NULL) {
		printf_char("\n此时链表中的元素为空！！！\n");
	}
	else {
		PNODE q = p->pnext;

		if (q == NULL) {
			printf_char("\n此时链表中的元素为空！！！\n");
		}
		else {
			printf_char("\n此时链表中各节点的元素为：");

			while (q != NULL) {
				printf("%d ", q->data);
				q = q->pnext;
			}

			printf_char("\n");
		}
	}

	return;
}

void judge_is_empty_list(PNODE p) {
	if (is_empty_list(p)) {
		printf_char("\n此时链表中的元素非空！！！\n");
	}
	else {
		printf_char("\n此时链表中的元素为空！！！\n");
	}

	return;
}

bool is_empty_list(PNODE p) {
	if (list_length(p) != 0) {
		return true;
	}
	else {
		return false;
	}
}

void user_insert_parameter(PNODE p) {
	int a = 0;
	int_t b = 0;

	printf_char("\n请输入元素想要插入此时链表范围内的位置：");
	scanf_s("%d", &a);

	printf("\n请输入向链表范围内第%d个位置此时想要插入的元素值：", a);
	scanf_s("%d", &b);

	judge_insert_list(p, a, b);

	return;
}

void judge_insert_list(PNODE p, int a, int_t b) {
	if (insert_list(p, a, b)) {
		printf("\n此时插入元素成功，插入的元素是 %d，存放的位置为 %d\n", b, a);
	}
	else {
		printf_char("\n此时插入元素失败！！！\n");
	}

	return;
}

bool insert_list(PNODE p, int a, int_t b) {
	if (a > list_length(p)) {
		printf_char("\n此时链表空间不满足想要插入数据的位置，请重新确认！！！\n");

		return false;
	}
	else if (a <= 0) {
		printf_char("\n此时输入想要插入的位置不合法，请重新确认！！！\n");

		return false;
	}
	else {
		PNODE q = p;

		if (a != 1) {
			int i = 1;

			while (i < a) {
				q = q->pnext;
				++i;
			}
		}

		PNODE pnew = NULL;
		pnew = (PNODE)malloc(sizeof(NODE));

		judge_creat_space(pnew);

		pnew->data = b;
		pnew->pnext = q->pnext;
		q->pnext = pnew;

		return true;
	}
}

void user_delete_parameter(PNODE p) {
	int a = 0;
	int_t b = 0;

	printf_char("\n请输入元素想要删除的位置：");
	scanf_s("%d", &a);

	judge_delete_list(p, a, &b);

	return;
}

void judge_delete_list(PNODE p, int a, int_t* b) {
	if (delete_list(p, a, b)) {
		printf("\n在此时的链表中删除第%d个节点成功，删除的节点元素是%d !\n", a, *b);
	}
	else {
		printf("\n在此时的链表中删除第%d个节点失败，删除的节点不存在！ \n", a);
	}

	return;
}

bool delete_list(PNODE p, int a, int_t* b) {
	if (a > list_length(p)) {
		printf_char("\n要删除的位置不存在，请重新确认！！！\n");

		return false;
	}
	else if (a <= 0) {
		printf_char("\n输入想要插入的位置不合法，请重新确认！！！\n");

		return false;
	}
	else {
		PNODE q = p;

		if (a != 1) {
			int i = 1;

			while (q->pnext != NULL && i < a) {
				q = q->pnext;
				++i;
			}
		}

		PNODE pnew = q->pnext;
		*b = pnew->data;
		q->pnext = pnew->pnext;

		free(pnew);

		return true;
	}
}

void sort_list(PNODE p){
	int i = 0, j = 0, t = 0;

	PNODE m = NULL;
	PNODE n = NULL;

	int length = list_length(p);

	if (length > 1){
		printf_char("\n此时对链表内容进行从大到小排序后\n");

		for (i = 0, m = p->pnext; i < length - 1; ++i, m = m->pnext){
			for (j = i + 1, n = m->pnext; j < length; ++j, n = n->pnext){
				if (m->data < n->data) {
					t = m->data;
					m->data = n->data;
					n->data = t;
				}
			}
		}
	}

	return;
}

int judge_destroy_list(PNODE p) {
	if (destroy_list(p)) {
		printf("\n此时链表格式化成功！！！\n");
	}
	else {
		printf("\n此时链表已经为空，无需格式化\n");
	}

	return 1;
}

bool destroy_list(PNODE p) {
	PNODE q = NULL;

	while (p != NULL) {
		q = p->pnext;

		if (q == NULL) {
			free(p);
			p = q;

			return true;
		}

		free(p);
		p = NULL;
		p = q;
	}

	return false;
}